\section{Realistic communication patterns} \label{sec:experiments}
So far we have only considered constructed and simple communication patterns. 
In this section we present results using the model on realistic communication patterns arising from a reservoir simulation application. 
This application involves solving a system of complicated partial differential equations on unstructured meshes. 
When we parallelize the reservoir simulation by partitioning the mesh and distributing each submesh among several processes, 
we introduce the need for inter-process nearest neighbour communication. 
This communication is needed to perform the SpMV operation, which is a crucial part of numerically solving the reservoir PDEs. 
Given a partition of the mesh we can create the communication pattern of the inter-process communication that will be needed 
when later performing SpMV operations. 
An example of such patterns is presented in Figure \ref{fig:heat}a. In this figure a realistic communication pattern is presented for a simulation with 128 processes, intended to be run on two Cavium nodes. 
Because we assume a straight forward mapping of the processes, we can classify each message as either on-socket, off-socket or inter-node. 
\\
\begin{figure}[ht!]
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.2,trim={2.1cm 0cm 3.3cm 1.8cm},clip]{figures/newCaseCore128.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
%\includegraphics[scale=0.22,trim={2cm 0cm 3.3cm 1.8cm},clip]{figures/
\includegraphics[scale=0.22,trim={0.5cm 0cm 1.5cm 1.8cm},clip]{figures/onOffCaviumCore.png}%newCaseCore128_size.png}
\subcaption{}
\end{subfigure}
\caption{Realistic 128 process communication pattern on 2 ARM Cavium nodes. The plot in (a) present the mapping, while the plot in (b) present total per-process communication volume.}
\label{fig:heat}
\end{figure}
\\
In Figure \ref{fig:heat}a we only present the send and receive relationship, and not the size of each individual message. 
In the plot in Figure \ref{fig:heat}b we display the number of bytes each process receives from its neighbours shown in Figure \ref{fig:heat}a. 
The amount of on-socket, off-socket and inter-node communication each process receives is also visible in the plot. 
We notice that the data is mostly received on-socket for this particular partitioning on the ARM Cavium nodes. 
We also observe that there is large variation in per-process communication volume between processes. 
\subsection*{Cavium, Kunpeng, Xeon-Gold and Epyc Rome experiments}
To test the model, we compare the execution time of Algorithm \ref{alg:cota} with estimated message transfer cost for varying number of processes on the four computing platforms.  
We want to use the measure in Eq. \ref{eq:err} to test the prediction accuracy of our model on the four available computing platforms. We start with Cavium, Kunpeng, Xeon-Gold and Epyc Rome. For these four CPUs we have access to at least four nodes. The communication pattern presented in Figure \ref{fig:heat} results in mostly on-socket communication. Because we also want to demonstrate the models performance when there is significant off-socket communication, we also include experiments where we use the openMPI option \texttt{--map-by socket}. Impact on per-process communication volume when using this option is shown in Figure \ref{fig:socketMap}.
\\
\begin{figure}[ht!]
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.25,trim={0.5cm 0cm 1.5cm 1.8cm},clip]{figures/onOffCaviumCore.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.25,trim={0.5cm 0cm 1.5cm 1.8cm},clip]{figures/onOffCaviumSocket.png}
\subcaption{}
\end{subfigure}
\caption{Per-process communication volume by type on two Cavium nodes, when running Algorithm \ref{alg:cota} with a realistic communication patterns using the \texttt{--map-by} option with core (a) or socket (b).}
\label{fig:socketMap}
\end{figure}
\\
Based on realistic communication patterns, such as the one presented in Figure \ref{fig:heat} and \ref{fig:socketMap}, we present prediction accuracy results attained on the Cavium, Kunpeng, Xeon-Gold and Epyc Rome nodes in Table \ref{tab:totRel1}. As seen in Figure \ref{fig:socketMap} using the \texttt{--map-by socket} options results in communication patterns with a good mix of on- and off-socket communication. We are therefore able to test the models prediction accuracy on realistic experiments where the majority of the message volume is off-socket.
\\
\begin{table}[ht!]
\caption{Prediction accuracy (using the Eq. \ref{eq:err} measure) on Cavium, Kunpeng, Xeon-Gold and Epyc Rome with realistic communication patterns. Results are attained on one, two and four nodes on both CPUs, corresponding to 64, 128 and 256 processes on Cavium, 128, 256 and 512 processes on Kunpeng and Epyc Rome and 52, 104 and 208 processes on Xeon-Gold. We also include results on a single socket, meaning 32, 64, 64 and 26 processes on Cavium, Kunpeng, Epyc Rome and Xeon-Gold, as well as a single NUMA domain on Kunpeng, corresponding to 32 processes. }
\label{tab:totRel1}
\begin{tabular}{| l | c | c | c | c | c | c |}
\hline
& $N$ & 32 & 64 & 128 & 256 & 512 \\
\hline
Cavium & \texttt{--map-by core} &  0.073& 0.076 & 0.097 &0.102  & -- \\
Cavium &\texttt{ --map-by socket} &  0.132 & 0.107 & 0.115 & 0.133  & -- \\
\hline
Kunpeng & \texttt{-map-by core} & 0.064 & 0.076 &0.081  & 0.064 & 0.1113 \\
Kunpeng & \texttt{--map-by socket} & 0.104& 0.086 &0.102  & 0.072 & 0.107 \\
\hline
Epyc Rome & \texttt{-map-by core}    & -- & 0.083 & 0.100 & 0.093 & 0.149\\
Epyc Rome & \texttt{-map-by socket} & -- & 0.084 & 0.122 & 0.109 & 0.149\\
\hline
& $N$ & 26 & 52 & 104 & 208 & -- \\
\hline
Xeon-Gold & \texttt{--map-by core} &  0.137 & 0.154 & 0.115 &0.177  & -- \\
Xeon-Gold &\texttt{ --map-by socket} &  0.178 &0.169 & 0.122 &  0.163 & -- \\
\hline
\end{tabular}
\end{table}
\\
The prediction accuracy presented in Table \ref{tab:totRel1} show that the model achieves good results for up to four Cavium and Kunpeng nodes. The total relative error ranges from 0.064 to 0.133 on Cavium and Kunpeng, and 0.115 to 0.178 on Xeon-Gold. A more detailed comparison for two Cavium and two Kunpeng nodes is presented in Figure \ref{fig:timeEst}. Here per-process execution time and model estimate are displayed in bar plots. In Figure \ref{fig:timeEst}a execution time and model estimate for 128 processes on two Cavium nodes are displayed, while the plot in Figure \ref{fig:timeEst}b display timing results and model estimates for 256 processes on two Kunpeng nodes. We observe good correspondence between model and execution time for most processes. 
\begin{figure}[ht!]
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.21,trim={2.1cm 0cm 3.3cm 1.8cm},clip]{figures/cavium128.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.21,trim={2cm 0cm 3.3cm 1.8cm},clip]{figures/hua256.png}
\subcaption{}
\end{subfigure}
\caption{The per-process execution time (blue bars) and model estimate (orange bars) for realistic communication patterns on two Cavium (a) and two Kunpeng (b) nodes.}
\label{fig:timeEst}
\end{figure}

\subsection*{AMD Rome experiments}
Because we have access to a cluster of AMD EPYC Rome nodes, we can test the prediction accuracy of our model on a larger scale, and on communication patterns that include a larger proportion of inter-node messages. We generate our realistic communicating pattern partitioning a model that is ten times the size of the model used to generate the communication patterns in the experiments on Cavium, Kunpeng and Xeon-Gold. A breakdown of per-node communication volume for this case when partitioning the model into 1024 and 8192 subdomains is displayed in Figure \ref{fig:betzyComVol}. 
\\
\begin{figure}[ht!]
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.2,trim={2cm 0cm 1.5cm 1.8cm},clip]{figures/betzyNodeCom1024.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.2,trim={2cm 0cm 1.5cm 1.8cm},clip]{figures/betzyNodeCom8192.png}
\subcaption{}
\end{subfigure}
\caption{Per-node communication volume when partitioning a realistic reservoir mesh into 1024 and 8192 subdomains on AMD EPYC Rome nodes.}
\label{fig:betzyComVol}
\end{figure}
\\
From Figure \ref{fig:betzyComVol}, we observe that the communication pattern attained by partitioning the larger model results in a majority of on-socket communication. However, we also notice that there is a significant amount of off-socket and inter-node communication too. The model accuracy for our AMD Rome experiments are presented in Table \ref{tab:betzyTotRel1}. In this table we include results attained on 4 to 64 nodes. The prediction accuracy displayed in Table \ref{tab:betzyTotRel1} ranges from 0.147 to 0.207. We therefore achieve quite good model estimates for our per-process model upto 8192 processes on 64 nodes. 
\\
\begin{table}[ht!]
\caption{Prediction accuracy (using the Eq. \ref{eq:err} measure) on AMD Epyc Rome nodes with realistic communication patterns. }
\label{tab:betzyTotRel1}
\begin{tabular}{| l | c | c | c | c | c | c | c | c |}
\hline
$N$ (nodes) & 512(4) & 640(5) & 768(6) & 896(7) & 1024(8) & 2048(16) & 4096(32) & 8192(64)\\
\hline
Prediction accuracy & 0.149 & 0.207 & 0.179 & 0.155 & 0.183& 0.147& 0.164 & 0.163 \\
\hline
\end{tabular}
\end{table}
\\

