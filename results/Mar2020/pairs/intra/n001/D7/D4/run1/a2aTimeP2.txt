 Data for JOB [48673,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [48673,1] App: 0 Process rank: 0 Bound: socket 1[core 57[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../../..]
 	Process OMPI jobid: [48673,1] App: 0 Process rank: 1 Bound: socket 1[core 34[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 5.18078e-06 5.17608e-06 
dune-size 2 3.91039e-06 3.91099e-06 
dune-size 4 4.06569e-06 4.06549e-06 
dune-size 8 4.25069e-06 4.24949e-06 
dune-size 16 4.68308e-06 4.68238e-06 
dune-size 32 5.63098e-06 5.63078e-06 
dune-size 64 7.19248e-06 7.19458e-06 
dune-size 128 1.05091e-05 1.05114e-05 
dune-size 256 1.75475e-05 1.75434e-05 
dune-size 512 3.07979e-05 3.08115e-05 
dune-size 1024 8.53952e-05 8.53992e-05 
dune-size 2048 0.000156081 0.00015607 
dune-size 4096 0.00029919 0.000299187 
dune-size 8192 0.000556224 0.000556226 
dune-size 16384 0.000884267 0.000884265 
dune-size 32768 0.00162839 0.00162835 
dune-size 65536 0.00323832 0.00323871 
dune-size 131072 0.00638744 0.00638799 
dune-size 262144 0.0127638 0.0127642 
dune-size 524288 0.0254166 0.0254091 
dune-size 1048576 0.050823 0.0508559 
