/*
  Copyright 2020 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_EDGECUTINFO_HEADER_INCLUDED
#define OPM_EDGECUTINFO_HEADER_INCLUDED

#endif // OPM_EDGECUTINFO_HEADER_INCLUDED

template<class G, class C>
void testCellPart(const G& grid, const C& cc, std::vector<int>& cell_part)
{
    int rank = cc.rank();
    auto gid = grid.globalIdSet();
    auto gv = grid.leafGridView();

    for (auto&& cell : elements(gv)) {
	if (cell.partitionType() == Dune::InteriorEntity) {

	    if (cell_part[gid.id(cell)] != rank)
		std::cout << "Wrong cell: " << gid.id(cell) << " in rank " << rank << std::endl; 
	}
    }
}

template<class G, class C>
void buildCellPart(const G& grid, const C& cc, std::vector<int>& cell_part)
{
    int numCells = grid.numCells();
    int totCells = 0;
    std::vector<int> igid;

    auto gid = grid.globalIdSet();

    auto gv = grid.leafGridView();

    for (auto&& cell : elements(gv)) {

	if (cell.partitionType() == Dune::InteriorEntity)
	    igid.push_back(gid.id(cell));
    }

    int rank = cc.rank();
    int mpi_size = cc.size();

    int numInterior = igid.size();
    int procNumCells[mpi_size];

    cc.gather(&numInterior, procNumCells, 1, 0);

    int start[mpi_size];
    int length[mpi_size];
    
    if (rank == 0) {
	start[0] = 0;
	length[0] = procNumCells[0];
	totCells = procNumCells[0];
	for (int i = 1; i < mpi_size ; ++i) {
	    start[i] = start[i-1] + procNumCells[i-1];
	    length[i] = procNumCells[i];
	    totCells += procNumCells[i];
	}
    }
    else {
	for (int i = 0; i < mpi_size ; ++i) {
	    start[i] = 0;
	    length[i] = 0;
	}
    }
    
    int* sendTab = igid.data();
    int entireTab[totCells];

    cc.gatherv(sendTab, numInterior, entireTab, length, start, 0);

    cell_part.resize(totCells);

    if (rank == 0) {
	for (int p = 0; p < mpi_size; ++p) {
	    for (int lid = start[p]; lid < start[p] + length[p]; lid++) {
		cell_part[entireTab[lid]] = p;
	    }
	}
    }

    cc.broadcast(&totCells, 1, 0);
    if (rank != 0)
	cell_part.resize(totCells);
    
    cc.broadcast(&cell_part[0], totCells, 0);

    testCellPart(grid, cc, cell_part);
}

template<class G, class C>
void evaluateEdgeCut(G& grid, const C& cc, const std::vector<int>& cell_part, const double* transmissibility)
{
    double wgtEc = 0.0;
    int ec = 0;
    
    if (cc.size() > 1) {
	grid.switchToGlobalView();
	if (cc.rank() == 0) {

	    
	    for (int face = 0; face < grid.numFaces(); face++) {
		double trans = transmissibility[face];

		int cellFace0 = grid.faceCell(face, 0);
		int cellFace1 = grid.faceCell(face, 1);

		if (cellFace0 != -1 && cellFace1 != -1) {
		    if ( cell_part[cellFace0] != cell_part[cellFace1] ) {
			ec++;
			wgtEc += trans;
		    }
		}
	    }
	}
	grid.switchToDistributedView();
    }

    if (cc.rank() == 0) {
	std::cout << std::endl;
	std::cout << "Edge-cut: " << ec << std::endl;
	std::cout << std::endl;
	std::cout << "Wgt-cut: " << wgtEc << std::endl;
	std::cout << std::endl;
    }
}
